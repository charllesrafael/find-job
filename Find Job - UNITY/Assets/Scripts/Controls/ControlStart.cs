﻿using Gaminho;
using UnityEngine;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour
{
    public Text Record;
    public Button BContinue;

    private PlayerData data;

    // Use this for initialization
    void Start () {
        //Reset the variables to start the game from scratch
        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.CurrentLevel = 0;

        Statics.Points = 0;
        Statics.ShootingSelected = 2;

        data = SaveController.LoadStage() as PlayerData;
        BContinue.interactable = data != null;

        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";

    }

    public void StartClick()
    {
        //Resetting saved data
        SaveController.SaveStage(
            new PlayerData { 
                stage = Statics.CurrentLevel,
                points = Statics.Points 
            }
        );
        LoadScene();
    }

    //Loading saved data
    public void Continue()
    {
        if (BContinue.interactable)
        {
            Statics.CurrentLevel = data.stage;
            Statics.Points = data.points;
        }
        LoadScene();
    }

    public void LoadScene()
    {
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
