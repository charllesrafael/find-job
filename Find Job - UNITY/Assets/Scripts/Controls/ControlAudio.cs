﻿using Gaminho;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ControlAudio : MonoBehaviour
{
    public static ControlAudio instance;
    private AudioSource sourceMusic;
    private List<AudioSource> audioSources;

    private void Awake()
    {
        instance = this;
        sourceMusic = GetComponent<AudioSource>();
        audioSources = new List<AudioSource>();
    }

    public void PlayMusic(AudioClip audioclip)
    {
        sourceMusic.Stop();
        sourceMusic.PlayOneShot(audioclip);
    }

    public void StopMusic()
    {
        sourceMusic.Stop();
    }

    public void PlayEffect(AudioClip audioclip)
    {
        bool itPlaying = false;

        for (int i = 0; i < audioSources.Count; i++)
        {
            if (!audioSources[i].isPlaying)
            {
                audioSources[i].PlayOneShot(audioclip);
                itPlaying = true;
            }
        }

        if (!itPlaying)
        {
            AudioSource audiosource = gameObject.AddComponent<AudioSource>();
            audioSources.Add(audiosource);
            audiosource.PlayOneShot(audioclip);
        }
    }

    internal void ControleAudio()
    {
        if (Statics.isPause)
            sourceMusic.Pause();
        else
            sourceMusic.UnPause();

        foreach (AudioSource item in audioSources)
        {
            if (Statics.isPause)
                item.Pause();
            else
                item.UnPause();
        }
    }
}
