using Gaminho;
using UnityEngine;
using UnityEngine.UI;

public class ControlPause : MonoBehaviour
{

    [SerializeField] private GameObject UIPause;
    [SerializeField] private Text UIPauseText;

    public void Pause()
    {
        Statics.isPause = !Statics.isPause;
        Time.timeScale = Statics.isPause ? 0 : 1;
        UIPause.SetActive(Statics.isPause);
        UIPauseText.text = !Statics.isPause ? "PAUSE" : "RESUME";
        ControlAudio.instance.ControleAudio();
    }
}