using Gaminho;
using UnityEngine;

public class CallLoadSave : MonoBehaviour
{
    public void LoadData(bool resetPoint = false)
    {
        PlayerData data = SaveController.LoadStage() as PlayerData;

        if (data != null)
        {
            Statics.CurrentLevel = data.stage;
            Statics.Points = resetPoint ? 0 : data.points;
        }
    }

    public void SaveData()
    {
        SaveController.SaveStage(
            new PlayerData
            {
                stage = Statics.CurrentLevel,
                points = Statics.Points
            }
        );
    }
}
