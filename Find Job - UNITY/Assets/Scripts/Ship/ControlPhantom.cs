using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPhantom : MonoBehaviour
{
    [SerializeField] private Transform ship;
    [SerializeField] private GameObject shield;
    [SerializeField] private GameObject shieldPhantom;
    [SerializeField] private Transform phantom;

    [SerializeField] private float delayPhantom = 1f;
    private WaitForSeconds delayPhantomWait;

    public struct dataPhantom
    {
        public Vector3 position;
        public Quaternion rotation;
        public bool shieldActive;
    }
    public List<dataPhantom> dataPhantomsTarget;
    private bool activeDelay = true;

    private void Start()
    {
        dataPhantomsTarget = new List<dataPhantom>();
        delayPhantomWait = new WaitForSeconds(delayPhantom);

        dataPhantomsTarget.Add(
            new dataPhantom
            {
                position = ship.position,
                rotation = ship.rotation,
                shieldActive = shield.activeSelf
            }
        );

        //Reset position and rotation of phantom
        phantom.position = ship.position;
        phantom.rotation = ship.rotation;
        shieldPhantom.SetActive(dataPhantomsTarget[0].shieldActive);

        StartCoroutine(Track());
        StartCoroutine(Move());
    }

    IEnumerator Track()
    {
        while (ship)
        {
            while (Statics.isPause)
            {
                yield return null;
            }

            dataPhantomsTarget.Add(
                new dataPhantom
                {
                    position = ship.position,
                    rotation = ship.rotation,
                    shieldActive = shield.activeSelf
                }
            );
            yield return null;
        }
    }

    IEnumerator Move()
    {
        yield return delayPhantomWait;
        while (ship)
        {
            while (Statics.isPause)
            {
                yield return null;
            }
            phantom.position = dataPhantomsTarget[0].position;
            phantom.rotation = dataPhantomsTarget[0].rotation;
            shieldPhantom.SetActive(dataPhantomsTarget[0].shieldActive);

            dataPhantomsTarget.Remove(dataPhantomsTarget[0]);

            yield return null;
        }

        //Goes to last player position and rotation, then "dies" 
        phantom.position = dataPhantomsTarget[0].position;
        phantom.rotation = dataPhantomsTarget[0].rotation;
        shieldPhantom.SetActive(dataPhantomsTarget[0].shieldActive);

        Destroy(phantom.gameObject);
    }
}


