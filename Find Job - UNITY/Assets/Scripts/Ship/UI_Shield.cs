using UnityEngine;

public class UI_Shield : MonoBehaviour
{
    [SerializeField] private GameObject shieldLife;
    [SerializeField] private Transform ship;
    [SerializeField] private Transform life;
    [SerializeField] private float offSet = 1f;
    private int maxLife = 10;

    private void FixedUpdate()
    {
        if(ship) 
            shieldLife.transform.position = ship.position + Vector3.up * offSet;
    }


    public void SetActiveLifeShield(bool active)
    {
        shieldLife.SetActive(active);
    }

    public void SetMaxLifeShield(int _maxlife)
    {
        maxLife = _maxlife;
    }

    public void SetLifeShield(int lifeValue)
    {
        float valorx = (float)lifeValue / maxLife;
        life.localScale = new Vector3(valorx, 1f, 1f );
    }
}
