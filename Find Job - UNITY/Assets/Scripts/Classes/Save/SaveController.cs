using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveController : MonoBehaviour
{
    private static string nameFile = "data.game";

    public static void SaveStage(object data)
    {
        string path = Application.persistentDataPath + nameFile;
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create); 

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static object LoadStage()
    {
        string path = Application.persistentDataPath + nameFile;

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            return formatter.Deserialize(stream);
        }

        return null;
    }
}
