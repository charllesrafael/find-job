﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000002 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000003 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000004 System.Void PlayerData::.ctor()
extern void PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B (void);
// 0x00000005 System.Void SaveController::SaveStage(System.Object)
extern void SaveController_SaveStage_mCB2E964758B9AC7824156B66F81623F1F0268839 (void);
// 0x00000006 System.Object SaveController::LoadStage()
extern void SaveController_LoadStage_m097245917D65AD15B13B7F2FF893D7E72AD7959B (void);
// 0x00000007 System.Void SaveController::.ctor()
extern void SaveController__ctor_m6ADEC7DD71D5FE677861C596F7AA059D316C83CA (void);
// 0x00000008 System.Void SaveController::.cctor()
extern void SaveController__cctor_m37B2524609805FEF0425ECE73912E1BA283D6169 (void);
// 0x00000009 System.Void ControlAudio::Awake()
extern void ControlAudio_Awake_mE0F566F1840B2ADD22C58937220DE731CDCBB0A2 (void);
// 0x0000000A System.Void ControlAudio::PlayMusic(UnityEngine.AudioClip)
extern void ControlAudio_PlayMusic_mDD5A79BF1AF92CAEA00B10A8E0EE40D9B65FDDF9 (void);
// 0x0000000B System.Void ControlAudio::StopMusic()
extern void ControlAudio_StopMusic_m0113C41854F2A3B8C1C09FA0B389455BB423C634 (void);
// 0x0000000C System.Void ControlAudio::PlayEffect(UnityEngine.AudioClip)
extern void ControlAudio_PlayEffect_m7CE6F106AA375E907C34572D5334FA99E1155EBE (void);
// 0x0000000D System.Void ControlAudio::ControleAudio()
extern void ControlAudio_ControleAudio_m56CC8F2C782255E466BF3B0E1B5B4DD111AB805C (void);
// 0x0000000E System.Void ControlAudio::.ctor()
extern void ControlAudio__ctor_m119D5D0D45869BCB256DFF5A01303B90A120C1D2 (void);
// 0x0000000F System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000010 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000011 System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x00000012 System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x00000013 System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x00000014 System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x00000015 System.Void ControlPause::Pause()
extern void ControlPause_Pause_m6DF83DB0F7EFC6857DD748EDFF2B83223278FDD0 (void);
// 0x00000016 System.Void ControlPause::.ctor()
extern void ControlPause__ctor_m308073180EB054F63BB6FC650AF23B9D2B127800 (void);
// 0x00000017 System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x00000018 System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x00000019 System.Void ControlStart::Continue()
extern void ControlStart_Continue_m3B448B76CFDA49B027673165D67AC25B437BFC8C (void);
// 0x0000001A System.Void ControlStart::LoadScene()
extern void ControlStart_LoadScene_m01571A6EF21774D17222997F04DBC0F39664E4B9 (void);
// 0x0000001B System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x0000001C System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x0000001D System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x0000001E System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x0000001F System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000020 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000021 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000022 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000023 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x00000024 System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x00000025 System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x00000026 System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x00000027 System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x00000028 System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x00000029 System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x0000002A System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x0000002B System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x0000002C System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x0000002D System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x0000002E System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x0000002F System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000030 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000031 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000032 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000033 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000034 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000035 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x00000036 System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x00000037 System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x00000038 System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x00000039 System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000003A System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000003B System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x0000003C System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x0000003D System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x0000003E System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x0000003F System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000040 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000041 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000042 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000043 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000044 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000045 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x00000046 System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x00000047 System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x00000048 UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x00000049 System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000004A System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000004B System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x0000004C System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x0000004D System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x0000004E System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x0000004F System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000050 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000051 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000052 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000053 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000054 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x00000055 System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x00000056 System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x00000057 System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x00000058 System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x00000059 System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000005A System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x0000005B System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x0000005C System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x0000005D System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x0000005E System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x0000005F System.Void Enemy::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Enemy_OnTriggerStay2D_mC5F712AE93386C5EF4DC8A764D0121AF31841454 (void);
// 0x00000060 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000061 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000062 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000063 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000064 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000065 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000066 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x00000067 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000068 System.Void Enemy/<KillMe>d__12::.ctor(System.Int32)
extern void U3CKillMeU3Ed__12__ctor_m694F0E215F331E8353A067625407747E65AEB418 (void);
// 0x00000069 System.Void Enemy/<KillMe>d__12::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__12_System_IDisposable_Dispose_m48AC9FDDF20647F16C11E5B8923E2C482B4E2EB8 (void);
// 0x0000006A System.Boolean Enemy/<KillMe>d__12::MoveNext()
extern void U3CKillMeU3Ed__12_MoveNext_mCA0ECCF5DA49E2529F9021EC28E28792D3E225C6 (void);
// 0x0000006B System.Object Enemy/<KillMe>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49856F7A9CDA530D51EC95176DA6010A4EEE6A4E (void);
// 0x0000006C System.Void Enemy/<KillMe>d__12::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__12_System_Collections_IEnumerator_Reset_mC239853130B0D15F5A47FA9CE732F6D98B68868E (void);
// 0x0000006D System.Object Enemy/<KillMe>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__12_System_Collections_IEnumerator_get_Current_mDD3F48C8B3E4BE00B2091FE6EB7A143955A12025 (void);
// 0x0000006E System.Void Enemy/<Shoot>d__14::.ctor(System.Int32)
extern void U3CShootU3Ed__14__ctor_m5AA8BC58A6C69CE1F1A61A286C5AC73C6DD7261D (void);
// 0x0000006F System.Void Enemy/<Shoot>d__14::System.IDisposable.Dispose()
extern void U3CShootU3Ed__14_System_IDisposable_Dispose_mDB3C9D44F575BA10CACEDA713A2229499433C6B4 (void);
// 0x00000070 System.Boolean Enemy/<Shoot>d__14::MoveNext()
extern void U3CShootU3Ed__14_MoveNext_m06148AE7BA8B861385162EAF7404237CFBB97912 (void);
// 0x00000071 System.Object Enemy/<Shoot>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5709BD886D86A0686C1974C1BE45D8251ED25BBC (void);
// 0x00000072 System.Void Enemy/<Shoot>d__14::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mD17A5F66DF4C23ABFCE94E21D30F9F1D7D26326F (void);
// 0x00000073 System.Object Enemy/<Shoot>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_mEC38A10547E706513CCA56494D702B9F2C4DB389 (void);
// 0x00000074 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000075 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000076 System.Void CallLoadSave::LoadData(System.Boolean)
extern void CallLoadSave_LoadData_m54DFA0236A22A5531701B54702DA375EC349B625 (void);
// 0x00000077 System.Void CallLoadSave::SaveData()
extern void CallLoadSave_SaveData_mB18257A2582E64C3B47A43220CECF716A8824E59 (void);
// 0x00000078 System.Void CallLoadSave::.ctor()
extern void CallLoadSave__ctor_m8D2B1316B38459486F9AAD6AB376C21EC2EE26FA (void);
// 0x00000079 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x0000007A System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x0000007B System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x0000007C System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000007D System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000007E System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000007F System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000080 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000081 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x00000082 System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000083 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000084 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000085 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000086 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000087 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000088 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000089 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x0000008A System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x0000008B System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x0000008C System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000008D Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x0000008E System.Void ControlPhantom::Start()
extern void ControlPhantom_Start_m6AFCC9F266BBA7051B03F766E09A9846E81EF422 (void);
// 0x0000008F System.Collections.IEnumerator ControlPhantom::Track()
extern void ControlPhantom_Track_m1C99B709E84D1C38ADC5C8C63F445E8642242670 (void);
// 0x00000090 System.Collections.IEnumerator ControlPhantom::Move()
extern void ControlPhantom_Move_m06C14BA0E7738807BA3708BC8BC641C731D01A52 (void);
// 0x00000091 System.Void ControlPhantom::.ctor()
extern void ControlPhantom__ctor_m370514DA4607173D66C0068DEA4300AACD8C0C7F (void);
// 0x00000092 System.Void ControlPhantom/<Track>d__10::.ctor(System.Int32)
extern void U3CTrackU3Ed__10__ctor_mF3237F138C051C24BBA63D416364E18835961986 (void);
// 0x00000093 System.Void ControlPhantom/<Track>d__10::System.IDisposable.Dispose()
extern void U3CTrackU3Ed__10_System_IDisposable_Dispose_mDF1E52A4303F414A5509E7EE0790956B4CE6B451 (void);
// 0x00000094 System.Boolean ControlPhantom/<Track>d__10::MoveNext()
extern void U3CTrackU3Ed__10_MoveNext_mF2F5CB0E02260EB29C90BFA30EAE01216B83805C (void);
// 0x00000095 System.Object ControlPhantom/<Track>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTrackU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E2C09D481225D38CC91682915B30AA0042BD66F (void);
// 0x00000096 System.Void ControlPhantom/<Track>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTrackU3Ed__10_System_Collections_IEnumerator_Reset_m8BA386AE7D5EB73A69D3B1E79C332BFC97E37BFA (void);
// 0x00000097 System.Object ControlPhantom/<Track>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTrackU3Ed__10_System_Collections_IEnumerator_get_Current_m93A1E6F3EFDD4005352254E23DCF990AFAC52DB4 (void);
// 0x00000098 System.Void ControlPhantom/<Move>d__11::.ctor(System.Int32)
extern void U3CMoveU3Ed__11__ctor_mABBFDEAEB8ADEA0873D945EA640A18E2C4AAD746 (void);
// 0x00000099 System.Void ControlPhantom/<Move>d__11::System.IDisposable.Dispose()
extern void U3CMoveU3Ed__11_System_IDisposable_Dispose_mCCDE8B192887FDA5D7E518AB9F44FA14ED47743F (void);
// 0x0000009A System.Boolean ControlPhantom/<Move>d__11::MoveNext()
extern void U3CMoveU3Ed__11_MoveNext_m9260E5321129405DF56B7133C934FD5FD53AED53 (void);
// 0x0000009B System.Object ControlPhantom/<Move>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1D4998FCC156481BD33264FAC562A317BE4F20F (void);
// 0x0000009C System.Void ControlPhantom/<Move>d__11::System.Collections.IEnumerator.Reset()
extern void U3CMoveU3Ed__11_System_Collections_IEnumerator_Reset_m4D74B99B46DCDEFA998577FA2F6CC5912D8CB4E1 (void);
// 0x0000009D System.Object ControlPhantom/<Move>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CMoveU3Ed__11_System_Collections_IEnumerator_get_Current_m5C99BC347643AEEE07F83F730ABB362CD3441000 (void);
// 0x0000009E System.Int32 ControlShip::get_LifeShield()
extern void ControlShip_get_LifeShield_m45AF962227C8A853F8ADAF23371BC308C2CD3465 (void);
// 0x0000009F System.Void ControlShip::set_LifeShield(System.Int32)
extern void ControlShip_set_LifeShield_m5CDB0A07AA1BD59D59412EAB07223180992F0E02 (void);
// 0x000000A0 System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x000000A1 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x000000A2 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x000000A3 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x000000A4 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x000000A5 System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x000000A6 System.Int32 ControlShip::GetLifeShield()
extern void ControlShip_GetLifeShield_m71F787300CA5E4E85BE4A100512FBAB63E6DAAB2 (void);
// 0x000000A7 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x000000A8 System.Void ControlShip/<Shoot>d__18::.ctor(System.Int32)
extern void U3CShootU3Ed__18__ctor_m85DB1A73258AB5D5787EC5111725670977F938A1 (void);
// 0x000000A9 System.Void ControlShip/<Shoot>d__18::System.IDisposable.Dispose()
extern void U3CShootU3Ed__18_System_IDisposable_Dispose_m8A0C98016668E05A5C5D41449E7ACEF7B5A57256 (void);
// 0x000000AA System.Boolean ControlShip/<Shoot>d__18::MoveNext()
extern void U3CShootU3Ed__18_MoveNext_mAE99FD2D4B48218E36B1CF0ACDE2BB1063FE3ECF (void);
// 0x000000AB System.Object ControlShip/<Shoot>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE908C3B525237077A7C0FE9C257C0043CA14805D (void);
// 0x000000AC System.Void ControlShip/<Shoot>d__18::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__18_System_Collections_IEnumerator_Reset_mB3ACD8661104619DBDAEBF9941F2B2A9A27873C7 (void);
// 0x000000AD System.Object ControlShip/<Shoot>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__18_System_Collections_IEnumerator_get_Current_mC4AEBD1C531AC3645417321F76C90BCC5B608C71 (void);
// 0x000000AE System.Void UI_Shield::FixedUpdate()
extern void UI_Shield_FixedUpdate_mC54612F0F9AA361FD05A1BB39175A8CA70266B02 (void);
// 0x000000AF System.Void UI_Shield::SetActiveLifeShield(System.Boolean)
extern void UI_Shield_SetActiveLifeShield_m4E15991F06C579B9B238CEFD0232D67E6001A95A (void);
// 0x000000B0 System.Void UI_Shield::SetMaxLifeShield(System.Int32)
extern void UI_Shield_SetMaxLifeShield_m1180C8AC5A34FF9764614B9B04E3D4790C6C38B5 (void);
// 0x000000B1 System.Void UI_Shield::SetLifeShield(System.Int32)
extern void UI_Shield_SetLifeShield_m38501F4E8AE6034E199E098E5FCF3B1C264FABCA (void);
// 0x000000B2 System.Void UI_Shield::.ctor()
extern void UI_Shield__ctor_mDCB187F1CB663183FACF9131144637FB371C1293 (void);
// 0x000000B3 System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x000000B4 System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x000000B5 System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x000000B6 System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x000000B7 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x000000B8 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[184] = 
{
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B,
	SaveController_SaveStage_mCB2E964758B9AC7824156B66F81623F1F0268839,
	SaveController_LoadStage_m097245917D65AD15B13B7F2FF893D7E72AD7959B,
	SaveController__ctor_m6ADEC7DD71D5FE677861C596F7AA059D316C83CA,
	SaveController__cctor_m37B2524609805FEF0425ECE73912E1BA283D6169,
	ControlAudio_Awake_mE0F566F1840B2ADD22C58937220DE731CDCBB0A2,
	ControlAudio_PlayMusic_mDD5A79BF1AF92CAEA00B10A8E0EE40D9B65FDDF9,
	ControlAudio_StopMusic_m0113C41854F2A3B8C1C09FA0B389455BB423C634,
	ControlAudio_PlayEffect_m7CE6F106AA375E907C34572D5334FA99E1155EBE,
	ControlAudio_ControleAudio_m56CC8F2C782255E466BF3B0E1B5B4DD111AB805C,
	ControlAudio__ctor_m119D5D0D45869BCB256DFF5A01303B90A120C1D2,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlPause_Pause_m6DF83DB0F7EFC6857DD748EDFF2B83223278FDD0,
	ControlPause__ctor_m308073180EB054F63BB6FC650AF23B9D2B127800,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_Continue_m3B448B76CFDA49B027673165D67AC25B437BFC8C,
	ControlStart_LoadScene_m01571A6EF21774D17222997F04DBC0F39664E4B9,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnTriggerStay2D_mC5F712AE93386C5EF4DC8A764D0121AF31841454,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__12__ctor_m694F0E215F331E8353A067625407747E65AEB418,
	U3CKillMeU3Ed__12_System_IDisposable_Dispose_m48AC9FDDF20647F16C11E5B8923E2C482B4E2EB8,
	U3CKillMeU3Ed__12_MoveNext_mCA0ECCF5DA49E2529F9021EC28E28792D3E225C6,
	U3CKillMeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49856F7A9CDA530D51EC95176DA6010A4EEE6A4E,
	U3CKillMeU3Ed__12_System_Collections_IEnumerator_Reset_mC239853130B0D15F5A47FA9CE732F6D98B68868E,
	U3CKillMeU3Ed__12_System_Collections_IEnumerator_get_Current_mDD3F48C8B3E4BE00B2091FE6EB7A143955A12025,
	U3CShootU3Ed__14__ctor_m5AA8BC58A6C69CE1F1A61A286C5AC73C6DD7261D,
	U3CShootU3Ed__14_System_IDisposable_Dispose_mDB3C9D44F575BA10CACEDA713A2229499433C6B4,
	U3CShootU3Ed__14_MoveNext_m06148AE7BA8B861385162EAF7404237CFBB97912,
	U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5709BD886D86A0686C1974C1BE45D8251ED25BBC,
	U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mD17A5F66DF4C23ABFCE94E21D30F9F1D7D26326F,
	U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_mEC38A10547E706513CCA56494D702B9F2C4DB389,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallLoadSave_LoadData_m54DFA0236A22A5531701B54702DA375EC349B625,
	CallLoadSave_SaveData_mB18257A2582E64C3B47A43220CECF716A8824E59,
	CallLoadSave__ctor_m8D2B1316B38459486F9AAD6AB376C21EC2EE26FA,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	ControlPhantom_Start_m6AFCC9F266BBA7051B03F766E09A9846E81EF422,
	ControlPhantom_Track_m1C99B709E84D1C38ADC5C8C63F445E8642242670,
	ControlPhantom_Move_m06C14BA0E7738807BA3708BC8BC641C731D01A52,
	ControlPhantom__ctor_m370514DA4607173D66C0068DEA4300AACD8C0C7F,
	U3CTrackU3Ed__10__ctor_mF3237F138C051C24BBA63D416364E18835961986,
	U3CTrackU3Ed__10_System_IDisposable_Dispose_mDF1E52A4303F414A5509E7EE0790956B4CE6B451,
	U3CTrackU3Ed__10_MoveNext_mF2F5CB0E02260EB29C90BFA30EAE01216B83805C,
	U3CTrackU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E2C09D481225D38CC91682915B30AA0042BD66F,
	U3CTrackU3Ed__10_System_Collections_IEnumerator_Reset_m8BA386AE7D5EB73A69D3B1E79C332BFC97E37BFA,
	U3CTrackU3Ed__10_System_Collections_IEnumerator_get_Current_m93A1E6F3EFDD4005352254E23DCF990AFAC52DB4,
	U3CMoveU3Ed__11__ctor_mABBFDEAEB8ADEA0873D945EA640A18E2C4AAD746,
	U3CMoveU3Ed__11_System_IDisposable_Dispose_mCCDE8B192887FDA5D7E518AB9F44FA14ED47743F,
	U3CMoveU3Ed__11_MoveNext_m9260E5321129405DF56B7133C934FD5FD53AED53,
	U3CMoveU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1D4998FCC156481BD33264FAC562A317BE4F20F,
	U3CMoveU3Ed__11_System_Collections_IEnumerator_Reset_m4D74B99B46DCDEFA998577FA2F6CC5912D8CB4E1,
	U3CMoveU3Ed__11_System_Collections_IEnumerator_get_Current_m5C99BC347643AEEE07F83F730ABB362CD3441000,
	ControlShip_get_LifeShield_m45AF962227C8A853F8ADAF23371BC308C2CD3465,
	ControlShip_set_LifeShield_m5CDB0A07AA1BD59D59412EAB07223180992F0E02,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip_GetLifeShield_m71F787300CA5E4E85BE4A100512FBAB63E6DAAB2,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__18__ctor_m85DB1A73258AB5D5787EC5111725670977F938A1,
	U3CShootU3Ed__18_System_IDisposable_Dispose_m8A0C98016668E05A5C5D41449E7ACEF7B5A57256,
	U3CShootU3Ed__18_MoveNext_mAE99FD2D4B48218E36B1CF0ACDE2BB1063FE3ECF,
	U3CShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE908C3B525237077A7C0FE9C257C0043CA14805D,
	U3CShootU3Ed__18_System_Collections_IEnumerator_Reset_mB3ACD8661104619DBDAEBF9941F2B2A9A27873C7,
	U3CShootU3Ed__18_System_Collections_IEnumerator_get_Current_mC4AEBD1C531AC3645417321F76C90BCC5B608C71,
	UI_Shield_FixedUpdate_mC54612F0F9AA361FD05A1BB39175A8CA70266B02,
	UI_Shield_SetActiveLifeShield_m4E15991F06C579B9B238CEFD0232D67E6001A95A,
	UI_Shield_SetMaxLifeShield_m1180C8AC5A34FF9764614B9B04E3D4790C6C38B5,
	UI_Shield_SetLifeShield_m38501F4E8AE6034E199E098E5FCF3B1C264FABCA,
	UI_Shield__ctor_mDCB187F1CB663183FACF9131144637FB371C1293,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[184] = 
{
	1237,
	1248,
	1457,
	1457,
	2337,
	2363,
	1457,
	2378,
	1457,
	1248,
	1457,
	1248,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1457,
	1425,
	1457,
	1425,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1457,
	1457,
	1457,
	1457,
	1425,
	1457,
	1425,
	1425,
	1248,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1457,
	1013,
	1425,
	1012,
	1425,
	1248,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1457,
	1457,
	1248,
	1248,
	1457,
	1425,
	1237,
	1425,
	1457,
	1457,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1248,
	1457,
	1265,
	1457,
	1457,
	1248,
	1457,
	1457,
	1425,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1457,
	1425,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	2357,
	1457,
	1425,
	1425,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1413,
	1237,
	1457,
	1457,
	1457,
	1425,
	1457,
	1248,
	1413,
	1457,
	1237,
	1457,
	1444,
	1425,
	1457,
	1425,
	1457,
	1265,
	1237,
	1237,
	1457,
	1457,
	1457,
	1457,
	1457,
	1888,
	2378,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	184,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
